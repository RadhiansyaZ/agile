$(document).ready(() => {
    findBooks("Sapiens");
    
    $('#search-bar').keypress(event => { 
        if(event.which == 13) {
            $('#search-button').click(); 
        }
    });

    $('#search-button').on('click', event => {
        let query = $('#search-bar').val();
        if(query == '') query = 'Sapiens';
        findBooks(query);
    });

    $("#response").on("click", ".laiku", (e => {
        //console.log(e.target.id);
        var csrftoken = $('[name="csrfmiddlewaretoken"]').attr('value')
        $.ajax({
            type: "POST",
            url: "/books/buku.json",
            data: {"id_buku":e.target.id},
            dataType: 'json',
            headers: {
                "X-CSRFToken": csrftoken,
                },
            success: response => {
                document.getElementById(e.target.id).innerHTML = response.items[0].likes;
            }
        })
    }));

    $.ajax({
        type: "GET",
        url: '/books/top5',
        success: eaaa => {
            eaaa.forEach(la => {
                const bookra = la.fields;
                //console.log(bookra);
                $('#modal-table').append(
                    $('.table').append(
                        $('<tr/>').append(
                            $('<td/>').append(
                                $('<img/>').attr('src', bookra.cover))
                    ).append(
                        $('<td/>').html(bookra.title)
                    ).append(
                        $('<td/>').html(bookra.author)
                    )
                )
                )
            });
        }
    });
            
    function findBooks(query) {
        //console.log("query masuk: " + query);
        $.ajax({ 
            type: "GET",
            url: '/books/buku.json?query=' + query,
            success: response => {
                //console.log(response)
                
                let number = 1;
                document.getElementById('response').innerHTML =
                `
                <table class="table table-dark table-hover">
                <thead class="thead-dark">
                    <tr>
                    <th style="width:10%;" scope="col">#</th>
                    <th style="width:25%;" scope="col" class="mx-auto text-center">Cover</th>
                    <th style="width:25%;" scope="col" class="text-center">Title</th>
                    <th style="width:25%;" scope="col" class="text-center">Author(s)</th>
                    <th style="width:15%;" scope="col" class="text-center">Likes</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
                </table>
                `;
                response.items.forEach(obj => {
                    const book_info = obj.volumeInfo;
                    const book = {
                        id: obj.id,
                        cover: book_info.imageLinks.smallThumbnail,
                        title: book_info.title,
                        author: book_info.authors,
                        likes: book_info.likes
                    }
                    let out = document.createElement("tr");
                    
                    // Number
                    let num = document.createElement("td");
                    num.innerText = number;       
                    num.setAttribute('class', 'align-middle');
                    out.appendChild(num);
    
                    // Cover
                    let coverOut = document.createElement("td");
                    let image = document.createElement("IMG");
                    image.src = book.cover;
                    coverOut.appendChild(image);
                    out.appendChild(coverOut);
    
                    // Title
                    let titleOut = document.createElement("td");
                    titleOut.innerText = book.title;
                    titleOut.setAttribute('class', 'align-middle text-center title');
                    out.appendChild(titleOut);
    
                    // Authors
                    let authorOut = document.createElement("td");
                    authorOut.innerText = book.author;
                    authorOut.setAttribute('class', 'align-middle text-center author');
                    out.appendChild(authorOut);
                    
                    // Likes                    
                    let likesOut = document.createElement("td");
                    likesOut.setAttribute('class', 'align-middle text-center');
                    out.appendChild(likesOut);
                    let likebut = document.createElement("button");
                    likebut.innerText = book.likes;
                    likebut.setAttribute('class', 'btn btn-outline-success laiku');
                    likebut.setAttribute('id',book.id);
                    likesOut.appendChild(likebut);
                    
                    document.getElementsByTagName("tbody")[0].appendChild(out);
    
                    number++;
                    });
            }
         })
    };
    
});

// Function for instant retrieve from Google Book API
// function findBooks(query) {
//     console.log("query masuk: " + query);
//     $.ajax({ 
//         type: "GET",
//         url: "https://www.googleapis.com/books/v1/volumes?q=" + String(query),
//         success: response => {
//             console.log(response)
//             let number = 1;
//             document.getElementById('response').innerHTML =
//             `
//             <table class="table" style="color: #ffffff;">
//             <thead class="thead-dark">
//                 <tr>
//                 <th scope="col">#</th>
//                 <th scope="col" class="mx-auto text-center">Cover</th>
//                 <th scope="col" class="text-center">Title</th>
//                 <th scope="col" class="text-center">Authors</th>
//                 <th scope="col" class="text-center">Likes</th>
//                 </tr>
//             </thead>
//             <tbody>
//             </tbody>
//             </table>
//             `;
//             response.items.forEach(obj => {
//                 let out = document.createElement("tr");

//                 // Number
//                 let num = document.createElement("td");
//                 num.innerText = number;       
//                 num.setAttribute('class', 'align-middle');
//                 out.appendChild(num);

//                 // Cover
//                 let coverOut = document.createElement("td");
//                 let image = document.createElement("IMG");
//                 image.src = obj.volumeInfo.imageLinks.smallThumbnail;
//                 coverOut.appendChild(image);
//                 out.appendChild(coverOut);

//                 // Title
//                 let titleOut = document.createElement("td");
//                 titleOut.innerText = obj.volumeInfo.title;
//                 titleOut.setAttribute('class', 'align-middle');
//                 out.appendChild(titleOut);

//                 // Authors
//                 let authorOut = document.createElement("td");
//                 authorOut.innerText = obj.volumeInfo.authors;
//                 authorOut.setAttribute('class', 'align-middle');
//                 out.appendChild(authorOut);

//                 document.getElementsByTagName("tbody")[0].appendChild(out);

//                 number++;
//                 });
//         }
//      })
// };

// });