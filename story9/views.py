from django.shortcuts import render
from django.http import JsonResponse, HttpResponseForbidden, HttpResponse
from .models import Books
import requests
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers

# Create your views here.
def books(request):
    return render(request,'books.html')

def fetch(request):
    if request.method == "GET":
        if request.is_ajax():
            response = requests.get('https://www.googleapis.com/books/v1/volumes?q=' + request.GET['query'])
            data = response.json()
            # Creating new json with likes
            for item in data['items']:
                try:
                    book = Books.objects.get(id=item['id'])
                    likes = book.likes
                except :
                    likes = 0
                item['volumeInfo']['likes'] = likes

            return JsonResponse(data)
        else:
            return HttpResponseForbidden()
    else:
        idBuku = request.POST.get('id_buku')
        books_json = requests.get("https://www.googleapis.com/books/v1/volumes?q=" + idBuku).json().get("items")[0]
        try:
            obj = Books.objects.get(id = idBuku)
            obj.likes += 1
            obj.save()
        except Books.DoesNotExist:
            obj = Books(
                id = idBuku,
                cover = books_json.get("volumeInfo").get("imageLinks").get("smallThumbnail"),
                title = books_json.get("volumeInfo").get("title"),
                author = books_json.get("volumeInfo").get("authors")[0],
                likes = 1
            )
            obj.save()
        context = [{"likes": obj.likes}]
        
        return JsonResponse({"items" : context})

def top5(request):
    if request.is_ajax():
        books5 = Books.objects.all()[:5]
        books5_parse = serializers.serialize('json', books5)
        return HttpResponse(books5_parse,content_type="text/json-comment-filtered")
    else:
        return HttpResponseForbidden()
