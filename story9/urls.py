from django.urls import path,include
from .views import books,fetch,top5

urlpatterns = [
    path('', books, name='books'),
    path('buku.json', fetch,name='fetch'),
    path('top5',top5,name='top5')
]