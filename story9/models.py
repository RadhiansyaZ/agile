from django.db import models

# Create your models here.
class Books(models.Model):
    id = models.CharField(max_length=127, primary_key=True)
    cover = models.CharField(max_length=255)
    title = models.CharField(max_length=255)
    author = models.CharField(max_length=255, blank=True)
    likes = models.PositiveIntegerField(default=0)

    class Meta:
        ordering = ('-likes',)