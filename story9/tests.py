from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve

from .views import books,fetch,top5
from .models import Books

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

# Create your tests here.
class UnitTest9(TestCase):
    def test_url_books(self):
        response = Client().get('/books/')
        self.assertEqual(response.status_code, 200)

    def test_func_books(self):
        found = resolve('/books/')
        self.assertEqual(found.func, books)

    def test_template_books(self):
        response = Client().get('/books/')
        self.assertTemplateUsed(response, 'books.html')

    def test_func_fetch(self):
        found = resolve('/books/buku.json')
        self.assertEqual(found.func, fetch)

    def test_model_books(self):
        prev = Books.objects.all().count()
        msg = Books.objects.create(id = "696969", cover = "Endgame", title="Noobgamers69", author="Thor", likes="69")
        current = Books.objects.all().count()
        self.assertEqual(prev + 1, current)

    def test_func_top5(self):
        found = resolve('/books/top5')
        self.assertEqual(found.func, top5)

class FuncTest9(LiveServerTestCase):
    def setUp(self):
        opt = Options()
        opt.add_argument('--no-sandbox') 
        opt.add_argument('--headless')
        opt.add_argument('disable-gpu')
        opt.add_argument('--disable-dev-shm-usage')
        self.browser = webdriver.Chrome(chrome_options = opt)
        super(FuncTest9, self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(FuncTest9, self).tearDown()

    def test_book_search(self):
        # The user want want to search a spesific books, from google's api book. So, S/he open the web
        self.browser.get(self.live_server_url + '/books/')
        self.browser.implicitly_wait(10)

        # User found a search box to find that book s/he wants
        # S/he likes non fiction books such as Harari's most popular writing, Sapiens.
        search_bar = self.browser.find_element_by_id('search-bar')
        search_bar.send_keys('Sapiens')
        search_bar.send_keys(Keys.ENTER)
        time.sleep(2)

        # After that, s/he found the book s/he wanted on screen
        text_in_page = self.browser.find_element_by_tag_name('body').text
        self.assertIn("Sapiens", text_in_page)
        self.assertIn("Yuval Noah Harari", text_in_page)

        # User wants to like a book, and hoping it will get greater in number
        # using the xPath
        like_sapiens = self.browser.find_element_by_xpath('//*[@id="FmyBAwAAQBAJ"]')
        like_counter_sapiens = self.browser.find_element_by_xpath('//*[@id="FmyBAwAAQBAJ"]').text
        like_sapiens.click()
        time.sleep(5)
        new_like_counter_sapiens = self.browser.find_element_by_xpath('//*[@id="FmyBAwAAQBAJ"]').text
        
        # The user sees the like counter increase
        self.assertGreater(int(new_like_counter_sapiens),int(like_counter_sapiens))

        # Now there is a button where when it clicked, show 5 most liked books
        modal = self.browser.find_element_by_class_name('modal-button')
        modal.click()
        time.sleep(5)
        # content_modal = self.browser.find_element_by_id('modal-table').text
        content_modal = self.browser.find_element_by_class_name('modal-content').text

        # Now the top 5 books modal opened
        self.assertIn("Top Books", content_modal)