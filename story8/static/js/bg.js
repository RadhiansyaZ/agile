$(document).ready(function() {
    $('button#color-toggle').on('click', function() {
        if($('body').hasClass('dark-mode')) {
            $('body').removeClass('dark-mode').addClass('light-mode');
        } else {
            $('body').removeClass('light-mode').addClass('dark-mode');
        }
        if($('#color-toggle').hasClass('btn-outline-secondary')) {
            $('#color-toggle').removeClass('btn-outline-secondary').addClass('btn-outline-info');
        } else {
            $('#color-toggle').removeClass('btn-outline-info').addClass('btn-outline-secondary');
        }
    });
});