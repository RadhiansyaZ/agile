$(document).ready(function() {
    $('button#color-toggle').on('click', function() {
        if($('#accordion > div > button').hasClass('dark-acc')) {
            $('#accordion > div > button').removeClass('dark-acc');
        } else {
            $('#accordion > div > button').addClass('dark-acc');
        }
    });
});