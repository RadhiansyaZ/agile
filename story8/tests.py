from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from .views import profile

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

# Create your tests here.
class UnitTest8(TestCase):
    def test_url_prof(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code, 200)

    def test_func_prof(self):
        found = resolve('/profile/')
        self.assertEqual(found.func, profile)

    def test_templ_prof(self):
        response = Client().get('/profile/')
        self.assertTemplateUsed(response, 'profile.html')

class FuncTest8(LiveServerTestCase):
    def setUp(self):
        opt = Options()
        opt.add_argument('--no-sandbox') 
        opt.add_argument('--headless')
        opt.add_argument('disable-gpu')
        opt.add_argument('--disable-dev-shm-usage')
        self.browser = webdriver.Chrome(chrome_options = opt)
        super(FuncTest8, self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(FuncTest8, self).tearDown()

    def test_accordion_click_clock(self):
        # The user open up the web page
        self.browser.get(self.live_server_url + '/profile/')
        time.sleep(3)
        # The user sees an accordion, which is a clickable object
        persona = self.browser.find_element_by_class_name('personal')
        persona.click()
        time.sleep(0.5)

        # User see someone personal information on expanded accordion
        contain_pers = self.browser.find_element_by_tag_name('body').text
        self.assertIn('Radhiansya Zain Antriksa Putra', contain_pers)
        self.assertIn('Universitas Indonesia', contain_pers)

        # The user wanna see different accordion so, s/he click on another accordion
        activity = self.browser.find_element_by_class_name('activity')
        activity.click()
        time.sleep(0.5)

        # The previous expanded information disappeared
        contain_pers = self.browser.find_element_by_tag_name('body').text
        self.assertNotIn('Radhiansya Zain Antriksa Putra', contain_pers)

        # Now the user saw someone activity
        self.assertIn('Kuliah', contain_pers)
        self.assertIn('Kuliah', contain_pers)
        self.assertIn('Nugas', contain_pers)
        self.assertIn('Apex', contain_pers)

        # The user wanna see different accordion so, s/he click on another accordion
        exp = self.browser.find_element_by_class_name('experience')
        exp.click()
        time.sleep(0.5)

        # The previous expanded information disappeared
        contain_pers = self.browser.find_element_by_tag_name('body').text
        self.assertNotIn('Apex', contain_pers)

        # Now the user saw someone past and current organization
        self.assertIn('COMPFEST', contain_pers)
        self.assertIn('BEM Fasilkom UI', contain_pers)
        self.assertIn('Pemira Fasilkom UI', contain_pers)

        # The user wanna see different accordion so, s/he click on another accordion
        achieve = self.browser.find_element_by_class_name('achievement')
        achieve.click()
        time.sleep(0.5)

        # The previous expanded information disappeared
        contain_pers = self.browser.find_element_by_tag_name('body').text
        self.assertIn('Pecundang', contain_pers)

        # Now, the user wants to change the layout of the accordion by clicking the arrow button
        prof_down_button = self.browser.find_element_by_xpath('/html/body/div/div[1]/div/div[1]/a[2]')
        prof_down_button.click()
        # Now, the accordion for profile is below the activity
        page_text = self.browser.find_element_by_tag_name('body').text
        self.assertTrue(page_text.find('Activity') < page_text.find('Personal Information'))
        # Down again
        prof_down_button = self.browser.find_element_by_xpath('/html/body/div/div[2]/div/div[1]/a[2]')
        prof_down_button.click()
        # Now, the accordion for profile is below the experience
        page_text = self.browser.find_element_by_tag_name('body').text
        self.assertTrue(page_text.find('Experience') < page_text.find('Personal Information'))
        # Orders : Activity, Experience, Personal Information, Achievement

        # The user click on up button on Achievement
        achieve.click()
        Achv_up_button = self.browser.find_element_by_xpath('/html/body/div/div[4]/div/div[1]/a[1]')
        Achv_up_button.click()
        page_text = self.browser.find_element_by_tag_name('body').text
        self.assertTrue(page_text.find('Achievement') < page_text.find('Personal Information'))
        # Orders : Activity, Experience, Achievement, Personal Information

        ###################################### Challenge 8 ###############################################

        # The background initially in dark theme
        color = self.browser.find_element_by_css_selector('body')
        self.assertEqual(color.get_attribute('class'), 'dark-mode')

        # User want to change it's web background color. Luckily, the web has a toggle to it
        switch = self.browser.find_element_by_id('color-toggle')
        switch.click()
        
        # User should see the theme is changing from dark to light
        color = self.browser.find_element_by_css_selector('body')
        self.assertEqual(color.get_attribute('class'), 'light-mode')

        # Now, the user thinks, can it be applied to the accordion too?
        acc_color = self.browser.find_element_by_css_selector('#accordion > div > button')
        self.assertIn('dark-acc', acc_color.get_attribute('class'))