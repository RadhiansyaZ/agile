from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve

from .views import index10,login10,signup,profile

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time
#AKHIRNYA STORY 10 !!!
# Create your tests here.
class UnitTest10(TestCase):
    def test_index_url(self):
        response = Client().get('/user/')
        self.assertEqual(response.status_code,200)

    def test_login_url(self):
        response = Client().get('/user/login/')
        self.assertEqual(response.status_code,200)
    
    def test_logout_url(self):
        response = Client().get('/user/logout/')
        self.assertEqual(response.status_code,302)

    def test_signup_url(self):
        response = Client().get('/user/signup/')
        self.assertEqual(response.status_code,200)

    def test_profile_up_url(self):
        response = Client().get('/user/profile-update/')
        self.assertEqual(response.status_code,200)

    def test_index_func(self):
        found = resolve('/user/')
        self.assertEqual(found.func,index10)

    def test_func_login(self):
        found = resolve('/user/login/')
        self.assertEqual(found.func,login10)

    def test_signup_func(self):
        found = resolve('/user/signup/')
        self.assertEqual(found.func,signup)

    def test_profile_up_func(self):
        found = resolve('/user/profile-update/')
        self.assertEqual(found.func,profile)

    def test_templ_index(self):
        response = Client().get('/user/')
        self.assertTemplateUsed(response,'user.html')

    def test_templ_login(self):
        response = Client().get('/user/login/')
        self.assertTemplateUsed(response,'login.html')
    
    def test_templ_signup(self):
        response = Client().get('/user/signup/')
        self.assertTemplateUsed(response,'signup.html')

    def test_templ_update(self):
        response = Client().get('/user/profile-update/')
        self.assertTemplateUsed(response, 'profile_up.html')

class FuncTest10(LiveServerTestCase):
    def setUp(self):
        opt = Options()
        opt.add_argument('--no-sandbox') 
        opt.add_argument('--headless')
        opt.add_argument('disable-gpu')
        opt.add_argument('--disable-dev-shm-usage')
        self.browser = webdriver.Chrome(chrome_options = opt)
        super(FuncTest10, self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(FuncTest10, self).tearDown()

    def test_signupu_login_logout_scene(self):
        self.browser.get(self.live_server_url + "/user/")
        self.browser.implicitly_wait(10)

        # The user wants to create an account so s/he click the signup button
        signup = self.browser.find_element_by_id('signup')
        signup.click()
        time.sleep(1)

        usern = self.browser.find_element_by_id('username') 
        pwd = self.browser.find_element_by_id('password')
        usern.send_keys('pensidota')
        pwd.send_keys('hanyadotatempatkembali')

        but_signup = self.browser.find_element_by_id('sub-signup')
        but_signup.click()
        time.sleep(1)

        # The user finishes creating his/her account so s/he wants to log in
        usern = self.browser.find_element_by_id('username') 
        pwd = self.browser.find_element_by_id('password')
        usern.send_keys('pensidota')
        pwd.send_keys('hanyadotatempatkembali')

        but_login = self.browser.find_element_by_id('sub-login')
        but_login.click()


        # The user done logged, thus seeing "Selamat datang his/her <username>"
        page_text = self.browser.find_element_by_tag_name('body').text
        self.assertIn('pensidota', page_text)

        profile = self.browser.find_element_by_id('sub-update')
        profile.click()

		# User change bio and profile picture
        bio = self.browser.find_element_by_name('bio')
        image = self.browser.find_element_by_name('image')
        bio.send_keys('gatau males pen beli truk')
        image.send_keys('https://iili.io/JcDXf4.jpg')
        time.sleep(3)
        image.send_keys(Keys.RETURN)
        time.sleep(3)

		# Now user bio and profile picture are displayed on homepage
        self.assertIn('gatau males pen beli truk', self.browser.page_source)

        # The user wants to log out and logged out
        but_logout = self.browser.find_element_by_id('sub-logout')
        but_logout.click()
        time.sleep(5)

        alert = self.browser.switch_to.alert
        alert.accept()
        time.sleep(1)

        page_text = self.browser.find_element_by_tag_name('body').text
        self.assertNotIn('pensidota', page_text)
