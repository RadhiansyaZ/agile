from django.urls import path,include
from .views import index10,login10,logout10,signup,profile

urlpatterns = [
    path('', index10, name='index10'),
    path('login/', login10,name='login'),
    path('logout/',logout10,name='logout'),
    path('signup/',signup,name='signup'),
    path('profile-update/',profile,name='profile-up')
]