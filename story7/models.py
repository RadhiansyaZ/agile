from django.db import models

# Create your models here.
WHITE = "ffffff"
BLUE = "81d4fa"
RED = "ffa4a2"
GREEN = "d7ffd9"
PURPLE = "d1d9ff"
COLOR_CHOICES = (
    (WHITE, "White"),
    (BLUE, "Blue"),
    (RED, "Red"),
    (GREEN, "Green"),
    (PURPLE, "Purple")
)

class Status(models.Model):
    name = models.CharField(max_length = 50)
    messages = models.TextField(max_length = 250)
    date_time = models.DateTimeField(auto_now_add = True, blank = True)
    color = models.CharField(max_length = 16, choices = COLOR_CHOICES, default = WHITE)

    def __str__(self):
        return self.messages