from django.shortcuts import render,HttpResponseRedirect, HttpResponse
from .forms import StatusForm
from .models import Status
import random
# Create your views here.
# Global variable to intechange the data in form between two views.
glob_name = ""
glob_post = ""
def index(request):
    if request.method == "POST":
        form = StatusForm(request.POST)
        global glob_name, glob_post
        nama = glob_name
        post = glob_post
        if nama != "" and post != "":
            new_post = Status(
                name = nama,
                messages = post
            )
            new_post.save()
            glob_name = ""
            glob_post = ""
    form = StatusForm()
    context = {
        'posts' : Status.objects.all().order_by('date_time').reverse().values(),
        'form' : form
    }
    return render(request, 'index.html',context)

def confirmation(request):
    global glob_name,glob_post
    if request.method == "POST":
        form = StatusForm(request.POST)
        nama = form.data['name_in_form']
        post = form.data['messages_in_form']
        context2 = {
        'name' : nama,
        'post' : post
        }
        glob_name = nama
        glob_post = post
        return render(request, 'confirmation.html', context2)
    else:
        nama = glob_name
        post = glob_post
        context2 = {
        'name' : nama,
        'post' : post
        }
        return render(request, 'confirmation.html', context2)

def change(request, pass_id):
    color = ['White','Blue','Red','Green','Purple']
    hex_color = {
        'White': "ffffff",
        'Blue': "81d4fa",
        'Red': "ffa4a2",
        'Green': "d7ffd9",
        'Purple': "d1d9ff"
    }
    post = Status.objects.get(pk=pass_id)
    current = post.color
    while current == post.color:
        select = random.choice(color)
        post.color = hex_color[select]
        post.save()
    return HttpResponseRedirect('/')