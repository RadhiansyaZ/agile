from django.urls import path
from .views import index, confirmation, change

urlpatterns = [
    path('', index, name='index'),
    path('confirm/', confirmation, name='confirmation'),
    path('change/<int:pass_id>', change, name='change')
]