from django import forms
from .models import Status

class StatusForm(forms.Form):
    name_in_form = forms.CharField(
        label = 'Name',
        required = True,max_length = 50,
        widget = forms.TextInput(attrs = {'class': 'form-control form-control-sm', 'style': 'background-color: #E1E2E1;','placeholder': 'Can I know you?'})
    )
    messages_in_form = forms.CharField(
        label = 'What\'s on your mind?',
        required = True,max_length = 250,
        widget = forms.Textarea(attrs = {'class': 'form-control form-control-sm', 'style': 'background-color: #E1E2E1;','placeholder': 'Share your thoughts.'})
    )

    class Meta:
        model = Status
        fields = ('name','messages')