from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from .views import index, confirmation, change
from .models import Status

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

# Create your tests here.
class UnitTest7(TestCase):
    def test_no_url_found(self):
        response = Client().get('/marvel_jelek/')
        self.assertEqual(response.status_code, 404)

    def test_url_index(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_function_index(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_template_index(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

    def test_model_pesan(self):
        prev = Status.objects.all().count()
        msg = Status.objects.create(name = "Alghi", messages = "Halo Dunia!")
        current = Status.objects.all().count()
        self.assertEqual(str(msg), msg.messages)
        self.assertEqual(prev + 1, current)

    def test_url_confirm(self):
        response = Client().get('/confirm/')
        self.assertEqual(response.status_code, 200)

    def test_function_confirm(self):
        found = resolve('/confirm/')
        self.assertEqual(found.func, confirmation)

    def test_template_index(self):
        response = Client().get('/confirm/')
        self.assertTemplateUsed(response, 'confirmation.html')

class UnitTestC7(TestCase):
    def test_url_color(self):
        Status.objects.create(name = "Alghi", messages = "Halo Dunia!")
        duar = Status.objects.get(name = "Alghi")
        response = Client().get('/change/' + str(duar.id))
        self.assertEqual(response.status_code, 302)

    def test_func_color(self):
        Status.objects.create(name = "Alghi", messages = "Halo Dunia!")
        duar = Status.objects.get(name = "Alghi")
        found = resolve('/change/' + str(duar.id))
        self.assertEqual(found.func, change)

    def test_model_pesan_color(self):
        prev = Status.objects.all().count()
        msg = Status.objects.create(name = "Alghi", messages = "Halo Dunia!", color = "White")
        current = Status.objects.all().count()
        self.assertEqual(str(msg), msg.messages)
        self.assertEqual(prev + 1, current)


class FunctionalTest7(LiveServerTestCase):
    def setUp(self):
        opt = Options()
        opt.add_argument('--headless')
        opt.add_argument('disable-gpu')
        self.browser = webdriver.Chrome(chrome_options = opt)
        super(FunctionalTest7, self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(FunctionalTest7, self).tearDown()

    def test_sel_fill_form(self):
        # User open the main pages containing form to create a post
        self.browser.get(self.live_server_url)
        time.sleep(1)

        # User send name and post content
        nawa = self.browser.find_element_by_id('id_name_in_form')
        nawa.send_keys("Uzumaki Naruto")
        msg = self.browser.find_element_by_id('id_messages_in_form')
        msg.send_keys("Halo Pacil, sekarang aku bisa TDD !!")

        # User clicks post button
        submit = self.browser.find_element_by_name('Posting')
        submit.send_keys(Keys.RETURN)
        time.sleep(2)

        # User Confirms it
        self.assertIn('Are you sure?', self.browser.page_source)
        post_it = self.browser.find_element_by_name('Submit')
        post_it.click()
        
        # Selenium will check whether inputs by User is successful or not
        self.assertIn("Uzumaki Naruto", self.browser.page_source)
        self.assertIn("Halo Pacil, sekarang aku bisa TDD !!", self.browser.page_source)

    def test_sel_fill_form_but_cancel(self):
        # User open the main pages containing form to create a post
        self.browser.get(self.live_server_url)
        time.sleep(1)

        # User send name and post content
        nawa = self.browser.find_element_by_id('id_name_in_form')
        nawa.send_keys("Uzumaki Naruto")
        msg = self.browser.find_element_by_id('id_messages_in_form')
        msg.send_keys("Halo Pacil, sekarang aku bisa TDD !!")

        # User clicks post button
        submit = self.browser.find_element_by_name('Posting')
        submit.send_keys(Keys.RETURN)
        time.sleep(2)

        # User Confirms it
        self.assertIn('Are you sure?', self.browser.page_source)
        post_it = self.browser.find_element_by_name('Abort')
        post_it.click()

        # Selenium will check whether inputs by User is canceled
        self.assertNotIn("Uzumaki Naruto", self.browser.page_source)
        self.assertNotIn("Halo Pacil, sekarang aku bisa TDD !!", self.browser.page_source)

    # Challenge 7 Functional Test
    def test_msg_color_change(self):
        # Create Dummy Objects for the Post
        Status.objects.create(name = "RadhiansyaZ", messages = "PPW HARAM.")

        # User open the main pages containing anchor to change color background
        self.browser.get(self.live_server_url)
        time.sleep(1)

        # Selenium get the current color
        current_color = self.browser.find_element_by_class_name('card').value_of_css_property('background-color')
        
        # Selenium click the anchor, thus changing the color
        henshin = self.browser.find_element_by_id('henshin')
        henshin.click()
        time.sleep(1)

        # Selenium get the new color
        new_color = self.browser.find_element_by_class_name('card').value_of_css_property('background-color')

        # Selenium will check whether the color is changed or not
        self.assertNotEqual(current_color, new_color)
